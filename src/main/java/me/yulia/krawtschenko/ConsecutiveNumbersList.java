package me.yulia.krawtschenko;

import java.util.ArrayList;

/**
 * An extension of NumberList for generating and operating on consecutive
 * integer values.
 * <p>
 * Implementation relies on the use of an internal ArrayList<Integer>.
 *
 * @author      Yulia Krawtschenko
 * @version     %I%, %G%
 * @since       1.0
 */
class ConsecutiveNumbersList extends NumbersList {
    /**
     * Class constructor. Generates an ArrayList<Integer> of consecutive
     * integers from lower to higher inclusive.
     * @param lower     the lower bound
     * @param higher    the higher bound
     */
    ConsecutiveNumbersList(final int lower, final int higher) {
        super();
        ArrayList<Integer> numbersList = new ArrayList<>();

        for (int i = lower; i <= higher; i++) {
            numbersList.add(i);
        }
        setList(numbersList);
    }

    /**
     * Uses the Gaussian method for computing the sum of consecutive integers,
     * adjusted to avoid the use of decimal values.
     * @return  the sum of elements in the number list
     */
    @Override
    public int getSumOfElements() {
        ArrayList<Integer> list = getList();
        int first = list.get(0);
        int last = list.get(list.size() - 1);

        if (isEven(list.size())) {
            return (list.size() / 2) * (first + last);
        } else {
            int middleIndex = list.size() / 2;

            return (((list.size() - 1) / 2) * (first + last))
                    + list.get(middleIndex);
        }
    }
}
