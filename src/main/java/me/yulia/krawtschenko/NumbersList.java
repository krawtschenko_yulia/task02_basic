package me.yulia.krawtschenko;

import java.util.ArrayList;

/**
 * NumberList is the base class for lists of elements of type int
 * which allow several convenient operations to be performed upon them, such
 * as:
 * <ul>
 * <li>Selecting even/odd elements
 * <li>Determining the percentages of even/odd elements in the list
 * <li>Computing the sum of all elements
 * <li>Finding the largest/smallest number present
 * </ul>
 * <p>
 * Implementation relies on the use of an internal ArrayList<Integer>.
 *
 * @author      Yulia Krawtschenko
 * @version     %I%, %G%
 * @since       1.0
 */
class NumbersList {
    /**
     * Inner representation of the numbers list.
     */
    private ArrayList<Integer> list;

    /**
     * Class constructor.
     */
    NumbersList() { }

    /**
     * Class constructor which provides the inner ArrayList<Integer>.
     * @param   numberList        provides the list proper
     */
    NumbersList(final ArrayList<Integer> numberList) {
        list = numberList;
    }

    /**
     * Getter for the inner ArrayList<Integer>.
     * @return  list        the inner representation of the numbers list
     */
    public ArrayList<Integer> getList() {
        return list;
    }

    /**
     * Setter for the inner ArrayList<Integer>.
     * @param numberList        provides the new value for the inner list
     */
    void setList(final ArrayList<Integer> numberList) {
        list = numberList;
    }

    /**
     * Method for filtering out odd numbers; creates a new ArrayList<Integer>.
     * @return  evens       an ArrayList<Integer> with only even numbers from
     *                      the original list
     */
    public ArrayList<Integer> getEvenElements() {
        ArrayList<Integer> evens = new ArrayList<>(list);
        evens.removeIf(number -> isOdd(number));

        return evens;
    }

    /**
     * Method for filtering out even numbers; creates a new ArrayList<Integer>.
     * @return  odds        an ArrayList<Integer> with only odd numbers from
     *                      the original list
     */
    public ArrayList<Integer> getOddElements() {
        ArrayList<Integer> odds = new ArrayList<>(list);
        odds.removeIf(number -> isEven(number));

        return odds;
    }

    /**
     * Method for calculating the percentage of odd numbers in the list.
     * @return  oddPercentage       not-rounded double value representing the
     *                              share of odd numbers in the given list
     */
    public double getOddElementsPercentage() {
        int numberOfOdds = 0;

        for (int number : list) {
            if (isOdd(number)) {
                numberOfOdds++;
            }
        }
        double oddsPercentage = numberOfOdds / (double) list.size();

        return oddsPercentage;
    }

    /**
     * Method for calculating the percentage of even numbers in the list.
     * @return  evensPercentage      not-rounded double value representing the
     *                              share of even numbers in the given list
     */
    public double getEvenElementsPercentage() {
        int numberOfEvens = 0;

        for (int number : list) {
            if (isEven(number)) {
                numberOfEvens++;
            }
        }
        double evensPercentage = numberOfEvens / (double) list.size();

        return evensPercentage;
    }

    /**
     * Method for calculating the total sum of the numbers in given list.
     * @return  sum     summed values of all list elements
     */
    public int getSumOfElements() {
        int sum = 0;

        for (int number : list) {
            sum += number;
        }

        return sum;
    }

    /**
     * Method for determining the largest numeric value in the list.
     * @return  max     the biggest number in the list
     */
    public int getBiggestElement() {
        int max = list.get(0);

        for (int number : list) {
            if (number > max) {
                max = number;
            }
        }

        return max;
    }

    /**
     * Method for determining the smallest numeric value in the list.
     * @return  min     the smallest number in the list
     */
    public int getSmallestElement() {
        int min = list.get(0);

        for (int number : list) {
            if (number < min) {
                min = number;
            }
        }

        return min;
    }

    /**
     * The method writes out the integers in the list to a String, in direct
     * order and one number per line. Overrides the parent toString() method.
     * @return  reversedList    string representation of list elements in
     *                          reversed order and one element per line
     */
    @Override
    public String toString() {
        StringBuilder numberList = new StringBuilder();

        for (Integer i : list) {
            numberList.append(i);
            numberList.append("\n");
        }
        numberList.deleteCharAt(numberList.length() - 1);
        String directList = numberList.toString();

        return directList;
    }

    /**
     * The method writes out the integers in the list to a String, in reversed
     * order and one number per line. Calls the parameter-less toString() method
     * if the parameter resolves to true.
     * @param   reversed        flag for whether the list should be reversed
     * @return  reversedList    string representation of list elements in
     *                          reversed order and one element per line
     */
    public String toString(final boolean reversed) {
        if (!reversed) {
            return toString();
        } else {
            StringBuilder numberList = new StringBuilder(toString());
            String reversedList = numberList.reverse().toString();

            return reversedList;
        }
    }

    /**
     * Utility method for determining parity of an integer.
     * @param number        integer to operate on
     * @return true if the number is even, false otherwise
     */
    protected boolean isEven(final int number) {
        return number % 2 == 0;
    }

    /**
     * Utility method for determining parity of an integer.
     * @param number        integer to operate on
     * @return true if the number is odd, false otherwise
     */
    protected boolean isOdd(final int number) {
        return number % 2 != 0;
    }
}
