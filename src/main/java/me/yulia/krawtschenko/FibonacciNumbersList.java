package me.yulia.krawtschenko;

import java.util.ArrayList;

/**
 * An extension of NumberList for generating and potentially operating on
 * consecutive integer values.
 * <p>
 * Implementation relies on the use of an internal ArrayList<Integer>.
 *
 * @author      Yulia Krawtschenko
 * @version     %I%, %G%
 * @since       1.0
 */
class FibonacciNumbersList extends NumbersList {
    /**
     * Class constructor. Generates an ArrayList<Integer> of Fibonacci
     * integer numbers.
     * @param size      size of the list to generate
     */
    FibonacciNumbersList(final int size) {
        super();
        ArrayList<Integer> numbersList = generateFibonacci(size);

        setList(numbersList);
    }

    /*
    private ArrayList<Integer> generateFibonacci(final int size) {
        ArrayList<Integer> list = new ArrayList<>();
        if (size >= 1) {
            list.add(0);
        }
        if (size >= 2) {
            list.add(1);
        }

        if (size > 2) {
            for (int i = 1; i < size; i++) {
                list.add(list.get(i - 1) + list.get(i));
            }
        }

        return list;
    }
   */

    /**
     * Generates an ArrayList<Integer> of Fibonacci numbers 0, 1, 1, 2,..
     * using a recursive strategy.
     * @param size      size of the list to generate
     * @return          list of Fibonacci numbers up to size
     */
    private ArrayList<Integer> generateFibonacci(final int size) {
        if (size > 2) {
            ArrayList<Integer> list = generateFibonacci(size - 1);
            int last = list.get(list.size() - 1);
            int oneBeforeLast = list.get(list.size() - 2);
            list.add(last + oneBeforeLast);

            return list;
        } else {
            ArrayList<Integer> list = new ArrayList<>();

            if (size >= 1) {
                list.add(0);
            }
            if (size == 2) {
                list.add(1);
            }

            return list;
        }
    }
}
