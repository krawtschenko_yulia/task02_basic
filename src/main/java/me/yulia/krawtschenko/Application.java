/**
 * Homework project on the core features of Java.
 * @author      Yulia Krawtschenko
 * @version     %I%, %G%
 * @since       1.0
 */
package me.yulia.krawtschenko;

import java.util.Scanner;

/**
 * Entry point class for the application. Demonstrates the functionality of the
 * project.
 * @author      Yulia Krawtschenko
 * @version     %I%, %G%
 * @since       1.0
 */
final class Application {
    /**
     * Class constructor.
     */
    private Application() { }

    /**
     * Main method.
     * @param args      command line arguments
     */
    public static void main(final String[] args) {
        Scanner input = new Scanner(System.in, "UTF-8");

        System.out.println("The program will generate a list of "
                + "consecutive integers from A to B, inclusive. ");
        System.out.print("Please enter the value for A: ");
        int lowerBound = input.nextInt();
        System.out.print("Please enter the value for B: ");
        int higherBound = input.nextInt();
        ConsecutiveNumbersList consecutive =
                new ConsecutiveNumbersList(lowerBound, higherBound);

        NumbersList oddNumbers = new NumbersList(consecutive.getOddElements());
        System.out.println("Odd (in direct order): ");
        System.out.println(oddNumbers);
        NumbersList evenNumbers =
                new NumbersList(consecutive.getEvenElements());
        System.out.println("Even (in reversed order): ");
        System.out.println(evenNumbers.toString(true));

        System.out.printf("The total sum of odd elements is equal to %d%n",
                oddNumbers.getSumOfElements());
        System.out.printf("The total sum of even elements is equal to %d%n",
                evenNumbers.getSumOfElements());

        System.out.println("The program will now generate a set of Fibonacci "
                + "numbers of size N, starting from 0. ");
        System.out.print("Please enter the value for N: ");
        int fibonacciSetSize = input.nextInt();
        FibonacciNumbersList fibonacci =
                new FibonacciNumbersList(fibonacciSetSize);

        System.out.printf("The percentage of odd numbers in the generated list "
                + "is %.2f%n", fibonacci.getOddElementsPercentage());
        System.out.printf("The percentage of even numbers in the generated "
                + "list is %.2f%n", fibonacci.getEvenElementsPercentage());

        System.out.printf("The largest odd number is %d%n",
                new NumbersList(fibonacci.getOddElements())
                        .getBiggestElement());
        System.out.printf("The largest even number is %d%n",
                new NumbersList(fibonacci.getEvenElements())
                        .getBiggestElement());
    }
}
